import {defineStore} from 'pinia';

interface moneyHistoryRequest{
  id : number
  dateMoneyRenewal : string //입출금일
  riderName : string // 라이더 이름
  moneyType : string // 입출금타입
  moneyAmount : number // 금액
  riderId : number //라이더 아이디
  etcMemo : string //메모
}

// board 스토어
export const useMoneyHistory = defineStore('moneyHistory', {
  state: () => ({
    moneys: [] as moneyHistoryRequest[],
    money: {}
  }),
  actions: {
    async fetchMoneyHistory() {
      const token = useCookie('token');
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/money-history/all/out/1`, {
          method: 'GET',
          headers: {
            'Authorization': `Bearer ${token}`
          },
        },
      );
      if (data.value) {
        this.moneys = data.value.list;
      }
    },
    async fetchNotice(moneyHistoryId: number) {
      // const token = useCookie('token');
      const { id} = useRoute().params
      const {data}:any = await useFetch(
        `http://matddak.shop:8080/v1/money-history/detail/${id}`, {
          method: 'GET',
          // headers: {
          //   'Authorization': `Bearer ${token}`
          // },
        },
      );
      console.log(data);
      if (data.value) {
        this.moneys = data.value.data;
      }
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useMoneyHistory, import.meta.hot))
}

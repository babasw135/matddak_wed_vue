import {defineStore} from 'pinia';

interface riderList {
    id: number
    name: string
    email: string
    dateJoin: string
    phoneNumber: string
    driveType: string
    addressWish: string
    admin: string
}

interface riderDetail {
    id: number
    name: string
    idNum: string
    confirmParents: string
    email: string
    password: string
    phoneNumber: string
    phoneType: string
    bankOwner: string
    bankName: string
    bankIdNum: string
    bankNumber: string
    addressWish: string
    driveType: string
    driveNumber: string
    dateJoin: string
    isBan: string
    reasonBan: string
    dateBan: string
    etcMemo: string
    admin: string
}

interface tablePage {
    totalCount: number
    totalPage: number
    currentPage: number
}


// rider 스토어
export const useRiderStore = defineStore('rider', {
    state: () => ({
        tableData: [] as riderDetail[],
        riders: {} as riderDetail[],
        totalCount: 0,
        totalPage: 0,
        currentPage:0,
    }),
    actions: {
        async fetchRider(pageNum:number) {
            const token = useCookie('token');
            const { data }:any = await useFetch(
                `http://matddak.shop:8080/v1/rider/all/${pageNum}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                });
            if (data) {
                this.tableData = data.value.list;
                this.currentPage = data.value.currentPage;
                // console.log(totalCountdata.value)
                this.totalPage = data.value.totalPage;
                this.totalCount = data.value.totalCount;
            }
        },

        async fetchBlack(){
            const token = useCookie('token');
            const { data }:any = await useFetch(
                `http://matddak.shop:8080/v1/rider/black-list/all`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                });
            if (data) {
                this.tableData = data.value.list;
            }
        },
        async getRiderDetail(riderId:number) {
            const token = useCookie('token');
            const { id} = useRoute().params;
            const { data }: any = await useFetch(
                `http://matddak.shop:8080/v1/rider/detail/${id}`, {
                    method: 'GET',
                    headers: {
                        'Authorization': `Bearer ${token}`
                    },
                }
            );

            if (data.value) {
                this.riders = data.value.data;
                console.log(data.value);
            }
        }
    }
})

if (import.meta.hot) {  //HMR
    import.meta.hot.accept(acceptHMRUpdate(useRiderStore, import.meta.hot))
}

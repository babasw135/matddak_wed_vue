import {defineStore} from 'pinia';
import type {INAdmin} from "~/interface";

interface adminRequest{
}

// board 스토어
export const useAdmin = defineStore('admin', {
  state: () => ({
    admins : {},
  }),
  actions: {
    setAdmin(data: INAdmin) {
      useFetch('http://matddak.shop:8080/v1/rider/join/admin', {
        method: 'POST',
        body: data
      })
    },
    async putAdmin(data: INAdmin) {
      const {id} = useRoute().params;
      useFetch(`http://matddak.shop:8080/v1/rider/change/1`, {
        method: 'PUT',
        body: data
      })
    },
    async fetchAdmin() {
      const token = useCookie('token');
      const {data}:any = await useFetch(
          `http://matddak.shop:8080/v1/rider/detail/1`, {
            method: 'GET',
            headers: {
              'Authorization': `Bearer ${token}`
            },
          },
      );
      if (data.value) {
        this.admins = data.value.data;
        console.log(data.value.data)
      }
    },
    async fetchNotice(adminId:number) {
      // const token = useCookie('token');
      const { id} = useRoute().params
      const {data}:any = await useFetch(
          `http://matddak.shop:8080/v1/delivery/detail/${id}`, {
            method: 'GET',
            // headers: {
            //   'Authorization': `Bearer ${token}`
            // },
          },
      );
      console.log(data);
      if (data.value) {
        this.admins = data.value;

      }
    }
  }
})

if (import.meta.hot) {  //HMR
  import.meta.hot.accept(acceptHMRUpdate(useAdmin, import.meta.hot))
}
